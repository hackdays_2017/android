package com.meetic.vision.vision

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.meetic.instagramapi.activities.InstagramAuthActivity
import com.meetic.instagramapi.engine.InstagramEngine
import com.meetic.instagramapi.engine.InstagramKitConstants
import com.meetic.instagramapi.exceptions.InstagramException
import com.meetic.instagramapi.interfaces.InstagramAPIResponseCallback
import com.meetic.instagramapi.objects.IGPagInfo
import com.meetic.instagramapi.objects.IGSession
import com.meetic.instagramapi.objects.IGUser
import com.meetic.instagramapi.utils.InstagramKitLoginScope
import com.meetic.vision.R
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : BaseFragment() {
    override fun getStatusBarColor(): Int {
        return android.R.color.darker_gray
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState, true)

        val scopes = arrayOf(InstagramKitLoginScope.BASIC,
                InstagramKitLoginScope.PUBLIC_ACCESS,
                InstagramKitLoginScope.LIKES,
                InstagramKitLoginScope.COMMENTS)

        val intent = Intent(activity, InstagramAuthActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        intent.putExtra(InstagramEngine.TYPE, InstagramEngine.TYPE_LOGIN)
        intent.putExtra(InstagramEngine.SCOPE, scopes)

        loginButton.setOnClickListener {
            startActivityForResult(intent, REQUEST_CODE_INSTAGRAM)
        }

        retryGetUserButton.setOnClickListener {
            getCurrentUser()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE_INSTAGRAM ->
                if (resultCode == Activity.RESULT_OK) {
                    val bundle = data?.extras
                    bundle?.let {
                        if (bundle.containsKey(InstagramKitConstants.kSessionKey)) {
                            val session: IGSession = bundle.getParcelable(InstagramKitConstants.kSessionKey)
                            getMember()?.instagramToken = session.accessToken
                            getCurrentUser()
                        }
                    }
                }
        }
    }

    private fun getCurrentUser() {
        loginViewFlipper?.displayedChild = DISPLAY_LOGIN_LOADING
        InstagramEngine.getInstance(context).getUserDetails(object : InstagramAPIResponseCallback<IGUser> {
            override fun onResponse(user: IGUser, pageInfo: IGPagInfo?) {
                getMember()?.let { account ->
                    account.username = user.username
                    account.pictureUrl = user.profilePictureURL
                }
                getMainActivity()?.showNextStep()
            }

            override fun onFailure(exception: InstagramException) {
                loginViewFlipper?.displayedChild = DISPLAY_LOGIN_RETRY
            }
        })
    }

    companion object {
        private const val REQUEST_CODE_INSTAGRAM = 33
        private const val DISPLAY_LOGIN_BUTTON = 0
        private const val DISPLAY_LOGIN_LOADING = 1
        private const val DISPLAY_LOGIN_RETRY = 2

        fun newInstance() = LoginFragment()
    }
}
