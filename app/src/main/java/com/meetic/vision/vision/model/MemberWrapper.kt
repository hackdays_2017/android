package com.meetic.vision.vision.model

import com.google.gson.annotations.SerializedName

class MemberWrapper(val status: String,
                    @SerializedName("data") val member: Member,
                    val message: String)