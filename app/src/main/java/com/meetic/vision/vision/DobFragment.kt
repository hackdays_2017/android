package com.meetic.vision.vision

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.meetic.vision.R
import com.meetic.vision.vision.client.VisionClient
import kotlinx.android.synthetic.main.fragment_dob.*

class DobFragment : BaseFragment() {

    override fun getStatusBarColor(): Int {
        return R.color.background_dob
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_dob, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState, false)

        dateOfBirth.minValue = 18
        dateOfBirth.maxValue = 99
        dateOfBirth.value = 28

        nextbutton.setOnClickListener {
            val visionClient = VisionClient()
            getMember()?.let { member ->
                member.year = dateOfBirth.value
                visionClient.login(
                        member = member.build(),
                        successCallback = { userId ->
                            // the user id returned by the server
                            member.id = userId
                            getMainActivity()?.showNextStep()
                        },
                        errorCallback = { cause ->
                            Log.e("DobFragment", "error with login:" + member.toString(), cause)
                            Toast.makeText(context, "ERROR", Toast.LENGTH_LONG).show()
                        })
            }
        }
    }

    companion object {
        fun newInstance() = DobFragment()
    }
}