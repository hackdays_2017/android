package com.meetic.vision.vision.custom

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import com.meetic.vision.R
import com.meetic.vision.vision.model.VisionLabel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_emoji_popup.view.*
import java.io.IOException


class EmojiPopupView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_emoji_popup, this, true)
    }

    private fun animateEmojis() {
        Handler().postDelayed({
            emoji_1.visibility = View.VISIBLE
            emoji_1.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }, 1000)
        Handler().postDelayed({
            emoji_7.visibility = View.VISIBLE
            emoji_7.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }, 1250)
        Handler().postDelayed({
            emoji_5.visibility = View.VISIBLE
            emoji_5.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }, 1500)
        Handler().postDelayed({
            emoji_4.visibility = View.VISIBLE
            emoji_4.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }, 1700)
        Handler().postDelayed({
            emoji_3.visibility = View.VISIBLE
            emoji_3.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }, 1800)
        Handler().postDelayed({
            emoji_6.visibility = View.VISIBLE
            emoji_6.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }, 1900)
        Handler().postDelayed({
            emoji_2.visibility = View.VISIBLE
            emoji_2.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }, 2000)
        Handler().postDelayed({
            emoji_8.visibility = View.VISIBLE
            emoji_8.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }, 2100)
    }

    fun bindPhoto(pictureUrl: String) {
        Picasso.with(context).load(pictureUrl).transform(com.meetic.vision.tinder.CircleTransform()).into(profilePhoto)
    }

    fun bindEmojis(visionLabels: List<VisionLabel>) {
        for ((index, value) in visionLabels.withIndex()) {
            try {
                val imageFilePath = value.uri()
                when (index) {
                    1 -> Picasso.with(context).load(imageFilePath).into(emoji_1)
                    2 -> Picasso.with(context).load(imageFilePath).into(emoji_2)
                    3 -> Picasso.with(context).load(imageFilePath).into(emoji_3)
                    4 -> Picasso.with(context).load(imageFilePath).into(emoji_4)
                    5 -> Picasso.with(context).load(imageFilePath).into(emoji_5)
                    6 -> Picasso.with(context).load(imageFilePath).into(emoji_6)
                    7 -> Picasso.with(context).load(imageFilePath).into(emoji_7)
                    8 -> Picasso.with(context).load(imageFilePath).into(emoji_8)
                }
            } catch (e: IOException) {

            }
        }
        animateEmojis()
    }
}

fun VisionLabel.uri() = "file:///android_asset/emojis/${this.emoji}.png"