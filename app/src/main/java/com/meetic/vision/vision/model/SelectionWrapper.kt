package com.meetic.vision.vision.model

import com.google.gson.annotations.SerializedName

class SelectionWrapper(val status: String,
                       @SerializedName("data") val selections: List<Member>,
                       val message: String)