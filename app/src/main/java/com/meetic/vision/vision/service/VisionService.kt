package com.meetic.vision.vision.service

import com.google.gson.annotations.SerializedName
import com.meetic.vision.vision.model.Member
import com.meetic.vision.vision.model.LoginResponse
import com.meetic.vision.vision.model.MemberWrapper
import com.meetic.vision.vision.model.SelectionWrapper
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path


interface VisionService {

    @POST("member")
    fun login(@Body member: Member): Call<LoginResponse>

    @GET("member/{id}")
    fun getMember(@Path("id") id: Int): Call<MemberWrapper>

    @GET("member/{id}/selection")
    fun getSelection(@Path("id") id: Int): Call<SelectionWrapper>

    @POST("like")
    fun postLike(@Body body: LikeBody): Call<LikeResponse>

}

data class LikeBody(
        @SerializedName("member_from") val memberFrom: Int,
        @SerializedName("member_to") val memberTo: Int,
        @SerializedName("kind") val kind: String //like, dislike, superlike
)

data class LikeResponse(

        @SerializedName("data") val data: String //ok, match,
)