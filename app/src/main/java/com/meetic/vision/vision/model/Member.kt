package com.meetic.vision.vision.model

import com.google.gson.annotations.SerializedName
import com.google.gson.internal.bind.util.ISO8601Utils
import java.util.*

data class Member(val id: Int,
                  val username: String,
                  @SerializedName("profile_picture_url")
                  val pictureUrl: String,
                  val kvk: String,
                  val birthdate: String,
                  @SerializedName("instagram_token")
                  val instagramToken: String,
                  val pictures: List<VisionPicture> = mutableListOf(),
                  @SerializedName("final_weight")
                  val finalWeight :Int = 1,
                  val labels:List<VisionLabel> = mutableListOf()
)

data class VisionPicture(val id: Int,
                         val url: String,
                         val labels: List<VisionLabel> = mutableListOf())

data class VisionLabel(val label: String,
                       val emoji: String)