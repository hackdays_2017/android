package com.meetic.vision.vision

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.meetic.vision.R
import kotlinx.android.synthetic.main.fragment_kvk.*

class KvkFragment : BaseFragment() {
    override fun getStatusBarColor(): Int {
        return R.color.background_kvk
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_kvk, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState, false)

        userGenderMale.setOnClickListener {
            setMe(isMale = true)
            userGenderMale.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }
        userGenderFemale.setOnClickListener {
            setMe(isMale = false)
            userGenderFemale.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }
        targetGenderMale.setOnClickListener {
            setTarget(isMale = true)
            targetGenderMale.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }
        targetGenderFemale.setOnClickListener {
            setTarget(isMale = false)
            targetGenderFemale.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
        }

        getMember()?.let { account ->
            setSelectionFromKvk(account.kvk)
        }

        nextButton.setOnClickListener {
            when (getMember()?.kvk) {
                in listOf("MF", "MM", "FM", "FF") -> {
                    getMainActivity()?.showNextStep()
                }
                else -> {
                    if (getMember()?.kvk?.first() == '0') {
                        showError("We need to know what you are")
                        userGenderMale.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
                        userGenderFemale.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
                    } else {
                        showError("We need to know what you are looking for")
                        targetGenderMale.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
                        targetGenderFemale.startAnimation(AnimationUtils.loadAnimation(context, R.anim.pulse))
                    }
                }
            }
        }
    }

    private fun showError(message: String) {
        errorTextView.text = message
        errorTextView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in))
        errorTextView.visibility = View.VISIBLE
    }

    private fun setSelectionFromKvk(kvk: String) {
        when (kvk[0]) {
            'M' -> setMe(isMale = true)
            'F' -> setMe(isMale = true)
        }
        when (kvk[1]) {
            'M' -> setTarget(isMale = true)
            'F' -> setTarget(isMale = true)
        }
    }

    private fun setMe(isMale: Boolean) {
        getMember()?.let { it.kvk = it.kvk.replaceRange(0, 1, if (isMale) "M" else "F") }
        userGenderMale.setImageResource(if (isMale) R.drawable.ic_man_hair else R.drawable.ic_man_hair_unselected)
        userGenderMale.setBackgroundResource(if (isMale) R.drawable.bg_white_circle_solid else R.drawable.bg_white_circle)
        userGenderFemale.setImageResource(if (!isMale) R.drawable.ic_woman_hair else R.drawable.ic_woman_hair_unselected)
        userGenderFemale.setBackgroundResource(if (!isMale) R.drawable.bg_white_circle_solid else R.drawable.bg_white_circle)
    }

    private fun setTarget(isMale: Boolean) {
        getMember()?.let { it.kvk = it.kvk.replaceRange(1, 2, if (isMale) "M" else "F") }
        targetGenderMale.setImageResource(if (isMale) R.drawable.ic_man_hair else R.drawable.ic_man_hair_unselected)
        targetGenderMale.setBackgroundResource(if (isMale) R.drawable.bg_white_circle_solid else R.drawable.bg_white_circle)
        targetGenderFemale.setImageResource(if (!isMale) R.drawable.ic_woman_hair else R.drawable.ic_woman_hair_unselected)
        targetGenderFemale.setBackgroundResource(if (!isMale) R.drawable.bg_white_circle_solid else R.drawable.bg_white_circle)
    }

    companion object {
        fun newInstance() = KvkFragment()
    }
}
