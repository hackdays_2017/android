package com.meetic.vision.vision.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(
        val status: String,
        @SerializedName("data") val userId: Int,
        val message: String
)