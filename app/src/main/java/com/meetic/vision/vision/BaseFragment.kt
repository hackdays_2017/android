package com.meetic.vision.vision

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.WindowManager
import com.meetic.vision.MainActivity
import com.meetic.vision.vision.model.MemberBuilder

abstract class BaseFragment : Fragment() {

    fun onViewCreated(view: View?, savedInstanceState: Bundle?, isTranslucent: Boolean) {
        super.onViewCreated(view, savedInstanceState)
        val window = activity.window
        if (!isTranslucent) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(activity, getStatusBarColor())
        } else {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }
    }

    abstract fun getStatusBarColor(): Int

    protected fun getMainActivity(): MainActivity? {
        return activity as? MainActivity
    }

    protected fun getMember(): MemberBuilder? = getMainActivity()?.member
}