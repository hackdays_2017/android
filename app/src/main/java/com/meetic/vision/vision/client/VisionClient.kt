package com.meetic.vision.vision.client

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.meetic.vision.BuildConfig
import com.meetic.vision.vision.model.LoginResponse
import com.meetic.vision.vision.model.Member
import com.meetic.vision.vision.model.MemberWrapper
import com.meetic.vision.vision.model.SelectionWrapper
import com.meetic.vision.vision.service.LikeBody
import com.meetic.vision.vision.service.LikeResponse
import com.meetic.vision.vision.service.VisionService
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class VisionClient {

    val okHttpClient: OkHttpClient = OkHttpClient.Builder().addNetworkInterceptor(StethoInterceptor()).build()

    val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.VISION_API_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    var service: VisionService = retrofit.create(VisionService::class.java)

    fun login(member: Member,
              successCallback: (userId: Int) -> Unit,
              errorCallback: (cause: Throwable) -> Unit) {
        val request = service.login(member)
        request.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                errorCallback(t?: Throwable("error from login"))
            }

            override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        response.body()?.userId?.let { successCallback(it) }
                    } else {
                        errorCallback(response.toThrowable())
                    }
                }
            }
        })
    }

    fun getMember(userId: Int,
                  successCallback: (member: Member) -> Unit,
                  errorCallback: (cause: Throwable) -> Unit) {
        val request = service.getMember(userId)
        request.enqueue(object : Callback<MemberWrapper> {
            override fun onResponse(call: Call<MemberWrapper>?, response: Response<MemberWrapper>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        response.body()?.member?.let { successCallback(it) }
                    } else {
                        errorCallback(response.toThrowable())
                    }
                }
            }

            override fun onFailure(call: Call<MemberWrapper>?, t: Throwable?) {
                errorCallback(t ?: Throwable("error from getMember"))
            }
        })
    }

    fun getSelection(id: Int, successCallback: (selections: List<Member>) -> Unit,
                     errorCallback: (cause: Throwable) -> Unit) {
        val request = service.getSelection(id)
        request.enqueue(object : Callback<SelectionWrapper> {
            override fun onResponse(call: Call<SelectionWrapper>?, response: Response<SelectionWrapper>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        response.body()?.selections?.let { successCallback(it) }
                    } else {
                        errorCallback(response.toThrowable())
                    }
                }
            }

            override fun onFailure(call: Call<SelectionWrapper>?, t: Throwable?) {
                errorCallback(t ?: Throwable("error from getSelection"))
            }
        })
    }

    fun like(likeBody : LikeBody, successCallback: (response : String) -> Unit,
                     errorCallback: (cause: Throwable) -> Unit) {
        val request = service.postLike(likeBody)
        request.enqueue(object : Callback<LikeResponse> {
            override fun onResponse(call: Call<LikeResponse>?, response: Response<LikeResponse>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        response.body()?.data?.let { successCallback(it) }
                    } else {
                        errorCallback(response.toThrowable())
                    }
                }
            }

            override fun onFailure(call: Call<LikeResponse>?, t: Throwable?) {
                errorCallback(t ?: Throwable("error from getSelection"))
            }
        })
    }

    fun <T : Any> Response<T>.toThrowable() = Throwable(this.code().toString() + " " + this.message())
}