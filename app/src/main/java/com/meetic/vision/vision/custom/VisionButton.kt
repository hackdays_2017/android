package com.meetic.vision.vision.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.meetic.vision.R
import kotlinx.android.synthetic.main.custom_vision_button.view.*

class VisionButton @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.custom_vision_button, this, true)

        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.customVisionButton)

            // Text
            if (a.hasValue(R.styleable.customVisionButton_visionButtonText)) {
                textView.text = a.getString(R.styleable.customVisionButton_visionButtonText)
            }

            // Icon
            if (a.hasValue(R.styleable.customVisionButton_visionButtonIcon)) {
                iconImageView.visibility = VISIBLE
                space.visibility = VISIBLE
                iconImageView.setImageDrawable(a.getDrawable(R.styleable.customVisionButton_visionButtonIcon))
            }
            a.recycle()
        }

    }

}
