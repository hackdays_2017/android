package com.meetic.vision.vision.model

import com.google.gson.internal.bind.util.ISO8601Utils
import java.util.*

class MemberBuilder {

    var id: Int = 0
    var username: String = ""
    var pictureUrl: String = ""
    var kvk: String = "00"
    var year: Int = 0
    var instagramToken: String = ""

    fun build(): Member = Member(id, username, pictureUrl, cleanKvk(), computeYearDate(), instagramToken)

    private fun cleanKvk() : String {
        var cleanKvk = ""
        cleanKvk += if (kvk[0] == 'M') 'M' else "F"
        cleanKvk += if (kvk[1] == 'M') "M" else "F"
        return cleanKvk
    }

    private fun computeYearDate() : String {
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.add(Calendar.YEAR, -year)
        return ISO8601Utils.format(calendar.time)
    }
}