package com.meetic.vision.vision

import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gelitenight.waveview.library.WaveView
import com.meetic.vision.R
import com.meetic.vision.WaveHelper
import com.meetic.vision.vision.client.VisionClient
import kotlinx.android.synthetic.main.fragment_wait_processing.*

class WaitProcessingFragment : BaseFragment() {

    override fun getStatusBarColor(): Int {
        return R.color.wave
    }

    private var waveHelper: WaveHelper? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_wait_processing, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState, false)
        waveHelper = WaveHelper(waveView)
        waveView.setShapeType(WaveView.ShapeType.SQUARE)
        waveView.setWaveColor(
                ContextCompat.getColor(activity, R.color.wave),
                ContextCompat.getColor(activity, R.color.wave_dark)
        )

        getMember()?.let { member ->
            // use member here
            emojiPopupView.bindPhoto(member.pictureUrl)

            val visionClient = VisionClient()
            visionClient.getMember(member.id,
                    successCallback = { member ->
                        val unEmptyLabels = member.pictures.flatMap {
                            it.labels
                        }.toSet()

                        emojiPopupView.bindEmojis(unEmptyLabels.filter {
                            it.emoji.length > 3
                        })
                    },
                    errorCallback = { cause ->
                        Log.e("WaitProcess", "error", cause)
                    })
        }

        // when processing is done call:
        Handler().postDelayed({
            getMainActivity()?.showNextStep()

        }, 5000)
    }

    override fun onPause() {
        super.onPause()
        waveHelper?.cancel()
    }

    override fun onResume() {
        super.onResume()
        waveHelper?.start()
    }

    companion object {
        fun newInstance() = WaitProcessingFragment()
    }

}