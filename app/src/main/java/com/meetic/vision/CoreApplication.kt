package com.meetic.vision

import android.app.Application
import com.facebook.stetho.Stetho

class CoreApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }

}