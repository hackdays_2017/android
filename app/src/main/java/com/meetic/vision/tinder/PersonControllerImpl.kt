package com.meetic.vision.tinder

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.meetic.vision.vision.client.VisionClient
import com.meetic.vision.vision.service.LikeBody

class PersonControllerImpl(private val context: Context, private val view: MatchView, private val id: Int) : PersonController {
    val visionClient = VisionClient()

    override fun onDislike(item: Person) {
        visionClient.like(LikeBody(id, item.id, "dislike"), successCallback = {r->
            Log.v("PersonControllerImpl", "work")
        },errorCallback = { cause ->
            Log.e("PersonControllerImpl", "error", cause)
        })
        show("It’s a no!")
    }

    override fun onLike(item: Person) {


        visionClient.like(LikeBody(id, item.id, "like"), successCallback = {r->
            if(r == "match"){
                visionClient.getMember(id,
                        successCallback = { member ->
                            view.match(
                                    member.toPerson(),
                                    item)
                        },
                        errorCallback = { cause ->
                            Log.e("PersonControllerImpl", "error", cause)
                        })
            }
        },errorCallback = { cause ->
            Log.e("PersonControllerImpl", "error", cause)
        })

    }

    override fun onSuperLike(item: Person) {
        visionClient.like(LikeBody(id, item.id, "superlike"), successCallback = {r->
            Log.v("PersonControllerImpl", "work")
        },errorCallback = { cause ->
            Log.e("PersonControllerImpl", "error", cause)
        })
        show("SUPER LIKE!")
    }

    override fun onIgnore(item: Person) {
        show("Not the right time ${item.name}")
    }

    private fun show(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}
