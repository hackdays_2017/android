package com.meetic.vision.tinder

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.graphics.Palette
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.meetic.vision.R
import com.meetic.vision.detail.DetailActivity
import com.meetic.vision.vision.client.VisionClient
import com.meetic.vision.vision.custom.uri
import com.meetic.vision.vision.model.Member
import com.meetic.vision.vision.model.VisionLabel
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.yuyakaido.android.cardstackview.CardStackView
import com.yuyakaido.android.cardstackview.SwipeDirection
import kotlinx.android.synthetic.main.activity_tinder.*
import kotlinx.android.synthetic.main.fragment_wait_processing.*

class TinderActivity : AppCompatActivity(), TinderView, MatchView {
    override fun displayError(cause: Throwable) {
        Log.e(TinderActivity::class.java.simpleName, "", cause)
        viewFlipper.displayedChild = 2
    }

    override fun displayList(persons: List<Person>) {
        if (persons.isEmpty()) {
            viewFlipper.displayedChild = 3
        } else {
            adapter.clear()
            adapter.addAll(persons)
            adapter.notifyDataSetChanged()
            Handler().postDelayed({ viewFlipper.displayedChild = 0 }, 2000)
        }
    }

    lateinit var target1: Target
    lateinit var target2: Target

    @SuppressLint("InflateParams")
    override fun match(p1: Person, p2: Person) {
        val view: View = LayoutInflater.from(this).inflate(R.layout.dialog_match, null, false)
        val gradientHolder = GradientHolder(0xFF567890.toInt(), 0xFFFF6666.toInt())
        view.background = GradientDrawable(GradientDrawable.Orientation.TL_BR, gradientHolder.toArray())
        target1 = PictureTarget(view.findViewById(R.id.OneImageView), view, gradientHolder, 0)
        target2 = PictureTarget(view.findViewById(R.id.TwoImageView), view, gradientHolder, 1)
        Picasso.with(this).load(p1.url).transform(com.meetic.vision.tinder.CircleTransform()).into(target1)
        Picasso.with(this).load(p2.url).transform(com.meetic.vision.tinder.CircleTransform()).into(target2)

        val alertDialog = AlertDialog.Builder(this)
                .setView(view)
                .setTitle(null)
                .create()
        view.findViewById<Button>(R.id.sendButton).setOnClickListener {
            this@TinderActivity.openChatWith(p2)
            alertDialog.dismiss()
        }
        view.findViewById<ImageButton>(R.id.closeButton).setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alertDialog.show()

    }

    class GradientHolder(var color1: Int, var color2: Int) {
        fun toArray() = intArrayOf(color1, color2)
    }

    class PictureTarget(val imageView: ImageView, val view: View, val gradientHolder: GradientHolder, val index: Int) : Target {
        override fun onBitmapFailed(errorDrawable: Drawable?) {
        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            bitmap?.let {
                imageView.setImageBitmap(bitmap)
                Palette.from(bitmap).generate {
                    it.vibrantSwatch?.let {
                        when (index) {
                            0 -> gradientHolder.color1 = it.rgb
                            1 -> gradientHolder.color2 = it.rgb
                        }
                        view.background = GradientDrawable(GradientDrawable.Orientation.TL_BR, gradientHolder.toArray())
                    }
                }
            }
        }

        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
        }
    }

    private fun openChatWith(p2: Person) {
        Log.v("TAG", "open chat with ${p2.name}")
    }

    private lateinit var adapter: ArrayAdapter<Person>

    private lateinit var controller: TinderController
    private lateinit var personController: PersonController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tinder)
        val id = intent.getIntExtra("id", -1)
        val username = intent.getStringExtra("username")
        val photoUrl = intent.getStringExtra("photoUrl")
        controller = TinderController(this, id)
        personController = PersonControllerImpl(this, this, id)
        adapter = TinderAdapter(this)
        cardStackView.setAdapter(adapter)
        cardStackView.setCardEventListener(object : CardStackView.CardEventListener {
            override fun onCardDragging(percentX: Float, percentY: Float) {
                // do nothing
            }

            override fun onCardSwiped(direction: SwipeDirection?) {
                direction?.let {
                    val topIndex = cardStackView.topIndex
                    if (topIndex == adapter.count) {
                        this@TinderActivity.onListFinish()
                    }
                    adapter.getItem(topIndex - 1)?.let {
                        this@TinderActivity.handlerDirectionFor(direction, it)
                    }
                }
            }

            override fun onCardReversed() {
            }

            override fun onCardMovedToOrigin() {
            }

            override fun onCardClicked(index: Int) {
                adapter.getItem(index)?.let {
                    startActivity(DetailActivity.newIntent(this@TinderActivity, it.id, it.name, it.url))
                }
            }

        })
        viewFlipper.displayedChild = 1
        retryButton.setOnClickListener {
            controller.get()
        }
        retryEmptyButton.setOnClickListener {
            controller.get()
        }
        controller.get()

        profileButton.setOnClickListener {
            startActivity(DetailActivity.newIntent(this@TinderActivity, id, username, photoUrl))
        }

    }

    private fun onListFinish() {
        viewFlipper.displayedChild = 1
        controller.get()
    }

    private fun handlerDirectionFor(direction: SwipeDirection, item: Person) {
        when (direction) {
            SwipeDirection.Left -> personController.onDislike(item)
            SwipeDirection.Right -> personController.onLike(item)
            SwipeDirection.Top -> personController.onSuperLike(item)
            SwipeDirection.Bottom -> personController.onIgnore(item)
        }
    }

    companion object {
        fun newIntent(context: Context, id: Int, name: String, pictureUrl: String): Intent {
            val intent = Intent(context, TinderActivity::class.java)
            intent.putExtra("id", id)
            intent.putExtra("username", name)
            intent.putExtra("photoUrl", pictureUrl)
            return intent
        }
    }

}

interface PersonController {
    fun onDislike(item: Person)
    fun onLike(item: Person)
    fun onSuperLike(item: Person)
    fun onIgnore(item: Person)
}


class TinderAdapter(context: Context) : ArrayAdapter<Person>(context, R.layout.item_tinder) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val item = getItem(position)
        val view: View
        val viewHolder: ViewHolder
        if (convertView == null) {
            view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_tinder, parent, false)
            viewHolder = ViewHolder(
                    view as CardView,
                    view.findViewById(R.id.theOneLayout),
                    view.findViewById(R.id.nameTextView),
                    view.findViewById(R.id.rate),
                    view.findViewById(R.id.details),
                    view.findViewById(R.id.emojisContainer),
                    view.findViewById(R.id.imageView)
            )
            view.tag = viewHolder

        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        viewHolder.theOneLayout.visibility = if (item.theOne) {
            View.VISIBLE
        } else {
            View.INVISIBLE
        }
        viewHolder.nameTextView.text = item.name
        viewHolder.rateTextView.text = item.rate
        viewHolder.detailTextView.text = "${item.town} ${item.age}"
        viewHolder.emojisContainer.removeAllViews()
        item.emojis.forEach {emoji ->
            val imageView : ImageView = LayoutInflater.from(view.context).inflate(R.layout.item_emoji, viewHolder.emojisContainer, false) as ImageView

            Picasso.with(view.context).load(emoji.uri()).into(imageView)
            viewHolder.emojisContainer.addView(imageView)
        }
        Picasso.with(view.context).load(item.url).into(viewHolder.imageView)
        return view
    }

    class ViewHolder(
            val view: CardView,
            val theOneLayout: ViewGroup,
            val nameTextView: TextView,
            val rateTextView: TextView,
            val detailTextView: TextView,
            val emojisContainer: ViewGroup,
            val imageView: ImageView
    )
}

class TinderController(

        private val view: TinderView,
        private val id: Int
) {
    fun get() {
        VisionClient().getSelection(id, successCallback = { selections:List<Member> ->
            val maxBy = selections.maxWith(Comparator { m1, m2 -> m1.finalWeight.compareTo(m2.finalWeight) })

            view.displayList(selections.map {

                it.toPerson(it.id == maxBy?.id?:-1, it.labels.filter {
                    it.emoji.length > 3
                })
            })
        }) { cause ->
            view.displayError(cause)
        }
        try {

        } catch (e: PersonsRepository.Error) {

        }
    }
}

interface PersonsRepository {
    fun get(): List<Person>
    class Error(message: String) : Throwable(message)
}

interface TinderView {
    fun displayList(persons: List<Person>)
    fun displayError(cause: Throwable)
}

data class Person(
        val name: String,
        val url: String,
        val emojis: List<VisionLabel>,
        val town: String,
        val age: String,
        val rate: String,
        val id: Int = -1,
        val theOne: Boolean = false
)

fun Member.toPerson(theOne:Boolean = false, listVisionLabel: List<VisionLabel> = emptyList())
        : Person = Person(
        name = this.username,
        url = this.pictureUrl,
        emojis = listVisionLabel,
        town = "",
        age = this.birthdate?:"",
        rate = "${this.finalWeight}",
        id = this.id,
        theOne = theOne
)

interface MatchView {
    fun match(p1: Person, p2: Person)
}