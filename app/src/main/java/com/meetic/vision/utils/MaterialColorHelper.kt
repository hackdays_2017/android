package com.meetic.vision.utils

class MaterialColorHelper {

    companion object {
        var materialColors = arrayOf("#B3FFEBEE", "#B3FFCDD2", "#B3EF9A9A", "#B3E57373", "#B3EF5350", "#B3F44336", "#B3E53935", //reds
                "#B3D32F2F", "#B3C62828", "#B3B71C1C", "#B3FF8A80", "#B3FF5252", "#B3FF1744", "#B3D50000", "#B3FCE4EC", "#B3F8BBD0", "#B3F48FB1", "#B3F06292", "#B3EC407A", "#B3E91E63", "#B3D81B60", //pinks
                "#B3C2185B", "#B3AD1457", "#B3880E4F", "#B3FF80AB", "#B3FF4081", "#B3F50057", "#B3C51162", "#B3F3E5F5", "#B3E1BEE7", "#B3CE93D8", "#B3BA68C8", "#B3AB47BC", "#B39C27B0", "#B38E24AA", //purples
                "#B37B1FA2", "#B36A1B9A", "#B34A148C", "#B3EA80FC", "#B3E040FB", "#B3D500F9", "#B3AA00FF", "#B3EDE7F6", "#B3D1C4E9", "#B3B39DDB", "#B39575CD", "#B37E57C2", "#B3673AB7", "#B35E35B1", //deep purples
                "#B3512DA8", "#B34527A0", "#B3311B92", "#B3B388FF", "#B37C4DFF", "#B3651FFF", "#B36200EA", "#B3E8EAF6", "#B3C5CAE9", "#B39FA8DA", "#B37986CB", "#B35C6BC0", "#B33F51B5", "#B33949AB", //indigo
                "#B3303F9F", "#B3283593", "#B31A237E", "#B38C9EFF", "#B3536DFE", "#B33D5AFE", "#B3304FFE", "#B3E3F2FD", "#B3BBDEFB", "#B390CAF9", "#B364B5F6", "#B342A5F5", "#B32196F3", "#B31E88E5", //blue
                "#B31976D2", "#B31565C0", "#B30D47A1", "#B382B1FF", "#B3448AFF", "#B32979FF", "#B32962FF", "#B3E1F5FE", "#B3B3E5FC", "#B381D4fA", "#B34fC3F7", "#B329B6FC", "#B303A9F4", "#B3039BE5", //light blue
//                "#B30288D1", "#B30277BD", "#B301579B", "#B380D8FF", "#B340C4FF", "#B300B0FF", "#B30091EA", "#B3E0F7FA", "#B3B2EBF2", "#B380DEEA", "#B34DD0E1", "#B326C6DA", "#B300BCD4", "#B300ACC1", //cyan
                "#B30097A7", "#B300838F", "#B3006064", "#B384FFFF", "#B318FFFF", "#B300E5FF", "#B300B8D4", "#B3E0F2F1", "#B3B2DFDB", "#B380CBC4", "#B34DB6AC", "#B326A69A", "#B3009688", "#B300897B", //teal
                "#B300796B", "#B300695C", "#B3004D40", "#B3A7FFEB", "#B364FFDA", "#B31DE9B6", "#B300BFA5", "#B3E8F5E9", "#B3C8E6C9", "#B3A5D6A7", "#B381C784", "#B366BB6A", "#B34CAF50", "#B343A047", //green
                "#B3388E3C", "#B32E7D32", "#B31B5E20", "#B3B9F6CA", "#B369F0AE", "#B300E676", "#B300C853", "#B3F1F8E9", "#B3DCEDC8", "#B3C5E1A5", "#B3AED581", "#B39CCC65", "#B38BC34A", "#B37CB342", //light green
                "#B3689F38", "#B3558B2F", "#B333691E", "#B3CCFF90", "#B3B2FF59", "#B376FF03", "#B364DD17", "#B3F9FBE7", "#B3F0F4C3", "#B3E6EE9C", "#B3DCE775", "#B3D4E157", "#B3CDDC39", "#B3C0CA33", //lime
//                "#B3A4B42B", "#B39E9D24", "#B3827717", "#B3F4FF81", "#B3EEFF41", "#B3C6FF00", "#B3AEEA00", "#B3FFFDE7", "#B3FFF9C4", "#B3FFF590", "#B3FFF176", "#B3FFEE58", "#B3FFEB3B", "#B3FDD835", //yellow
                "#B3FBC02D", "#B3F9A825", "#B3F57F17", "#B3FFFF82", "#B3FFFF00", "#B3FFEA00", "#B3FFD600", "#B3FFF8E1", "#B3FFECB3", "#B3FFE082", "#B3FFD54F", "#B3FFCA28", "#B3FFC107", "#B3FFB300", //amber
                "#B3FFA000", "#B3FF8F00", "#B3FF6F00", "#B3FFE57F", "#B3FFD740", "#B3FFC400", "#B3FFAB00", "#B3FFF3E0", "#B3FFE0B2", "#B3FFCC80", "#B3FFB74D", "#B3FFA726", "#B3FF9800", "#B3FB8C00", //orange
                "#B3F57C00", "#B3EF6C00", "#B3E65100", "#B3FFD180", "#B3FFAB40", "#B3FF9100", "#B3FF6D00", "#B3FBE9A7", "#B3FFCCBC", "#B3FFAB91", "#B3FF8A65", "#B3FF7043", "#B3FF5722", "#B3F4511E", //deep orange
                "#B3E64A19", "#B3D84315", "#B3BF360C", "#B3FF9E80", "#B3FF6E40", "#B3FF3D00", "#B3DD2600", "#B3EFEBE9", "#B3D7CCC8", "#B3BCAAA4", "#B3A1887F", "#B38D6E63", "#B3795548", "#B36D4C41", //brown
//                "#B35D4037", "#B34E342E", "#B33E2723", "#B3FAFAFA", "#B3F5F5F5", "#B3EEEEEE", "#B3E0E0E0", "#B3BDBDBD", "#B39E9E9E", "#B3757575", //grey
                "#B3616161", "#B3424242", "#B3212121", "#B3ECEFF1", "#B3CFD8DC", "#B3B0BBC5", "#B390A4AE", "#B378909C", "#B3607D8B", "#B3546E7A", //blue grey
                "#B3455A64", "#B337474F", "#B3263238")

        fun generateColorFromName(name: String): String {
            return materialColors[Math.abs(name.hashCode()) % materialColors.size]
        }

        fun generateColorFromPosition(position: Int): String {
            return generateColorFromName("" + position)
        }
    }

}


