package com.meetic.vision

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.meetic.vision.tinder.TinderActivity
import com.meetic.vision.vision.DobFragment
import com.meetic.vision.vision.KvkFragment
import com.meetic.vision.vision.LoginFragment
import com.meetic.vision.vision.WaitProcessingFragment
import com.meetic.vision.vision.model.MemberBuilder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val member: MemberBuilder = MemberBuilder()
    private var profileCaptureStep = 0
    private val profileCaptureFlow = listOf(
            Pair<Fragment, Int>(LoginFragment.newInstance(), R.color.background_kvk),
            Pair<Fragment, Int>(KvkFragment.newInstance(), R.color.background_kvk),
            Pair<Fragment, Int>(DobFragment.newInstance(), R.color.background_dob),
            Pair<Fragment, Int>(WaitProcessingFragment.newInstance(), android.R.color.transparent)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showFragment(profileCaptureFlow[profileCaptureStep].first)
    }

    fun showNextStep() {
        profileCaptureStep++
        if (profileCaptureFlow.size > profileCaptureStep) {
            showFragment(profileCaptureFlow[profileCaptureStep].first, profileCaptureStep > 1)
            animateBackground(profileCaptureFlow[profileCaptureStep - 1].second, profileCaptureFlow[profileCaptureStep].second)
        } else {
            startActivity(TinderActivity.newIntent(this, member.id, member.username, member.pictureUrl))
            finish()
        }
    }

    private fun animateBackground(colorFrom: Int, colorTo: Int) {
        ObjectAnimator.ofObject(root, "backgroundColor", ArgbEvaluator(),
                ContextCompat.getColor(this, colorFrom),
                ContextCompat.getColor(this, colorTo))
                .setDuration(BACKGROUND_COLOR_ANIMATION_DURATION)
                .start()
    }

    private fun showFragment(fragment: Fragment, addToStack: Boolean = false) {
        val transaction = supportFragmentManager.beginTransaction()
        if (addToStack) {
            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
            transaction.addToBackStack(null)
        }
        transaction.replace(R.id.fragmentContainer, fragment)
        transaction.commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (profileCaptureStep > 0) {
            animateBackground(profileCaptureFlow[profileCaptureStep].second, profileCaptureFlow[profileCaptureStep - 1].second)
            profileCaptureStep--
        }
    }

    companion object {
        private const val BACKGROUND_COLOR_ANIMATION_DURATION = 500L
    }

}
