package com.meetic.vision.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.meetic.vision.R
import kotlinx.android.synthetic.main.activity_detail.*
import android.support.v7.widget.RecyclerView
import com.meetic.vision.tinder.CircleTransform
import com.meetic.vision.vision.client.VisionClient
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity() {

    private var userId: Int = 0
    private var name: String? = null
    private var userPictureUrl: String? = null
    private val visionClient = VisionClient()
    private lateinit var pictureAdapter: UserPicturesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        pictureAdapter = UserPicturesAdapter(this)

        if (intent?.hasExtra(EXTRA_USER_ID) ?: false) {
            userId = intent.getIntExtra(EXTRA_USER_ID, 0)
        }
        if (intent?.hasExtra(EXTRA_USER_NAME) ?: false) {
            name = intent.getStringExtra(EXTRA_USER_NAME)
            userName.text = name
        }
        if (intent?.hasExtra(EXTRA_USER_PICTURE_URL) ?: false) {
            userPictureUrl = intent.getStringExtra(EXTRA_USER_PICTURE_URL)
        }

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@DetailActivity)
            adapter = pictureAdapter
        }

        retryButton.setOnClickListener {
            viewFlipper.displayedChild = DISPLAY_LOADING
            fetchMember()
        }

        closeButtonContainer.setOnClickListener { finish() }

        var totalScrolled = 0
        val imageHeight = resources.getDimension(R.dimen.picture_detail_height)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                totalScrolled += dy
                if (totalScrolled < imageHeight) {
                    profilePhotoContainer.alpha = 1.0f - (((totalScrolled.times(100.0f)) / imageHeight)).div(100.0f)
                    userName.alpha = 1.0f - (((totalScrolled.times(100.0f)) / imageHeight)).div(100.0f)
                } else {
                    profilePhotoContainer.alpha = .0f
                    userName.alpha = .0f
                }
            }
        })

        fetchMember()
    }

    private fun fetchMember() {
        visionClient.getMember(userId,
                successCallback = {
                    member ->
                    Picasso.with(this).load(member.pictureUrl).transform(CircleTransform()).into(profilePhotoContainer)
                    // When picture are here
                    pictureAdapter.addItems(member.pictures)
                    viewFlipper.displayedChild = DISPLAY_LIST
                },
                errorCallback = {
                    viewFlipper.displayedChild = DISPLAY_ERROR
                })
    }

    companion object {
        private const val EXTRA_USER_ID = "EXTRA_USER_ID"
        private const val EXTRA_USER_NAME = "EXTRA_USER_NAME"
        private const val EXTRA_USER_PICTURE_URL = "EXTRA_USER_PICTURE_URL"
        private const val DISPLAY_LOADING = 0
        private const val DISPLAY_LIST = 1
        private const val DISPLAY_ERROR = 2

        fun newIntent(context: Context, userId: Int, name: String, pictureUrl: String): Intent {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(EXTRA_USER_ID, userId)
            intent.putExtra(EXTRA_USER_NAME, name)
            intent.putExtra(EXTRA_USER_PICTURE_URL, pictureUrl)
            return intent
        }
    }

}