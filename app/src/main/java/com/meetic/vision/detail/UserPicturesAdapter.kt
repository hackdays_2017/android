package com.meetic.vision.detail

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.meetic.vision.R
import com.meetic.vision.utils.MaterialColorHelper
import com.meetic.vision.vision.model.VisionPicture
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cell_detail_picture.view.*

class UserPicturesAdapter(context: Context) : RecyclerView.Adapter<UserPicturesAdapter.ViewHolder>() {

    private val data = mutableListOf<VisionPicture>()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.cell_detail_picture, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(position, item)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int, item: VisionPicture) {
            itemView.visibility = if (position < 1) View.INVISIBLE else View.VISIBLE
            if (item.url.isNotEmpty()) {
                Picasso.with(itemView.context)
                        .load(item.url)
                        .into(itemView.pictureImageView)
            }

            // Tags
            itemView.cardContentOverlay.removeAllViews()
            for (i in 0..2) {
                if (item.labels.size > i) {
                    val tagLine = TagLineEmoji(itemView.context)
                    tagLine.setLabel(item.labels[i])
                    itemView.cardContentOverlay.addView(tagLine)
                }
            }
            itemView.cardContentOverlay.setBackgroundColor(Color.parseColor(MaterialColorHelper.generateColorFromName(item.url)))
        }

    }

    fun addItems(items: List<VisionPicture>) {
        data.add(0, VisionPicture(0, ""))
        data.addAll(items)
        notifyDataSetChanged()
    }

}