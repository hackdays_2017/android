package com.meetic.vision.detail

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.meetic.vision.R
import com.meetic.vision.vision.model.VisionLabel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_tag_line_emoji.view.*

class TagLineEmoji @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.custom_tag_line_emoji, this, true)
    }

    fun setLabel(label: VisionLabel) {
        textView.text = label.label
        if (label.emoji != " ") {
            emojiView.visibility = View.VISIBLE
            val img = label.emoji
            Picasso.with(textView.context).load("file:///android_asset/emojis/$img.png").into(emojiView)
        } else {
            emojiView.visibility = View.INVISIBLE
        }
    }

}