package com.meetic.instagramapi.widgets;

import android.os.Handler;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.meetic.instagramapi.interfaces.InstagramAuthCallbackListener;

/**
 * Created by Sayyam on 3/18/16.
 */
public class InstagramWebViewClient extends WebViewClient {

    InstagramAuthCallbackListener instagramAuthCallbackListener;

    private InstagramWebViewClient() {
    }

    public InstagramWebViewClient(InstagramAuthCallbackListener instagramAuthCallbackListener) {
        this.instagramAuthCallbackListener = instagramAuthCallbackListener;
    }

    @Override
    public void onPageFinished(final WebView view, String url) {
        // do your stuff here
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                view.setVisibility(View.VISIBLE);
            }
        }, 500);

    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {

        if (!instagramAuthCallbackListener.onRedirect(url)) {
            view.loadUrl(url);
        }

        return true;
    }


}
