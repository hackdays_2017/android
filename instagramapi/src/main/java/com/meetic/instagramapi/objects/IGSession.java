package com.meetic.instagramapi.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class IGSession implements Parcelable {

    private String accessToken;

    public IGSession() {
    }

    public IGSession(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.accessToken);
    }

    protected IGSession(Parcel in) {
        this.accessToken = in.readString();
    }

    public static final Parcelable.Creator<IGSession> CREATOR = new Parcelable.Creator<IGSession>() {
        @Override
        public IGSession createFromParcel(Parcel source) {
            return new IGSession(source);
        }

        @Override
        public IGSession[] newArray(int size) {
            return new IGSession[size];
        }
    };
}
