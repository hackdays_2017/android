package com.meetic.instagramapi.interfaces;

import com.meetic.instagramapi.objects.IGPagInfo;
import com.meetic.instagramapi.exceptions.InstagramException;

/**
 * Created by Sayyam on 3/18/16.
 */
public interface InstagramAPIResponseCallback<T> {

    public void onResponse(T responseObject, IGPagInfo pageInfo);

    public void onFailure(InstagramException exception);
}
